import { useSession } from "next-auth/react";
import Link from "next/link";
import Spinner from "./Spinner";
import { signIn, signOut } from "next-auth/react";

interface Props {
    children: React.ReactNode
}

const Navbar: React.FC<Props> = ({ children }: Props) => {
    const { data: session, status } = useSession();

    if (status === "loading") {
        return (
            <Spinner />
        )
    }

    return (
        <>
            <nav className="px-5">
                <h1 className="text-3xl pt-4 font-bold underline">Guestbook</h1>
                <div className="items flex space-x-2 pb-2">
                    <Link href="/">
                        <span>Home</span>
                    </Link>
                    <Link href="/privacy">
                        <span>Privacy Policy</span>
                    </Link>
                </div>

                {session ? (
                    <><h1 className="font-bold text-xl">Welcome, {session.user?.name}!</h1><button className="bg-blue-500 p-2 rounded-md" onClick={() => signOut()}>Log out</button></>
                ) : (
                    <button className="bg-blue-500 p-2 rounded-md" onClick={() => signIn("discord")}>
                        Login with Discord
                    </button>
                )}
            </nav>


            {children}
        </>
    )
}

export default Navbar;