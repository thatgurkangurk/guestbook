import classNames from "classnames";
import toast from "react-hot-toast";
import { BiCheckCircle, BiErrorCircle } from "react-icons/bi";
import { MdOutlineClose } from "react-icons/md";

import errorStyles from '../styles/Error.module.css'
import successStyles from '../styles/Success.module.css'

export const error = ({ title, text }: {
    title: string,
    text: string
  }) =>
    toast.custom(
      (t) => (
        <div
          className={classNames([
            errorStyles.errorWrapper,
            t.visible ? "top-0" : "-top-96",
          ])}
        >
          <div className={errorStyles.iconWrapper}>
            <BiErrorCircle />
          </div>
          <div className={errorStyles.contentWrapper}>
            <h1>{title}</h1>
            <p>
              {text}
            </p>
          </div>
          <div className={errorStyles.closeIcon} onClick={() => toast.dismiss(t.id)}>
            <MdOutlineClose />
          </div>
        </div>
      ),
      { id: "error", position: "top-center" }
);

export const success = ({ title, text }: {
    title: string,
    text: string
  }) =>
    toast.custom(
      (t) => (
        <div
          className={classNames([
            successStyles.successWrapper,
            t.visible ? "top-0" : "-top-96",
          ])}
        >
          <div className={successStyles.iconWrapper}>
            <BiCheckCircle />
          </div>
          <div className={successStyles.contentWrapper}>
            <h1>{title}</h1>
            <p>
              {text}
            </p>
          </div>
          <div className={successStyles.closeIcon} onClick={() => toast.dismiss(t.id)}>
            <MdOutlineClose />
          </div>
        </div>
      ),
      { id: "success", position: "top-center" }
);