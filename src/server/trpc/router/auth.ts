import { router, publicProcedure, protectedProcedure } from "../trpc";
import { z } from "zod";

export const authRouter = router({
  getSession: publicProcedure.query(({ ctx }) => {
    return ctx.session;
  }),
  getSecretMessage: protectedProcedure.query(() => {
    return "you can now see this secret message!";
  }),
  getIsAdmin: publicProcedure
  .input(
    z.object({
      userId: z.string().optional(),
    })
  )
  .query(async ({ ctx, input }) => {
    const { prisma } = ctx;
    if (!input.userId) return false;
    const user = await prisma.user.findUnique({
      where: {
        id: input.userId
      }
    })

    if (!user) return false;

    if (user.isAdmin) return true;
  })
});
