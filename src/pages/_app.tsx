import { type AppType } from "next/app";
import { type Session } from "next-auth";
import { SessionProvider } from "next-auth/react";

import { trpc } from "../utils/trpc";

import "../styles/globals.css";
import Navbar from "../components/Navbar";
import Script from "next/script";
import Link from "next/link";

const MyApp: AppType<{ session: Session | null }> = ({
  Component,
  pageProps: { session, ...pageProps },
}) => {
  return (
    <>
      <Script
        async
        defer
        data-website-id="126e8e5-2bbf-4837-a61c-797190a4d7fc"
        src="https://analytics.gurkz.me/umami.js" />
      <nav className="p-4 border-b-2 border-b-gray-500">
        <Link href="https://gurkz.me">
          Lost? Go back home
        </Link>
      </nav>
      <SessionProvider session={session}>
        <Navbar>
          <Component {...pageProps} />
        </Navbar>
      </SessionProvider>
    </>
  );
};

export default trpc.withTRPC(MyApp);
