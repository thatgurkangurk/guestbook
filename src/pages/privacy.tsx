import Link from "next/link";

const PrivacyPolicy = () => {
    return <>
        <main className="py-3 px-5">
            <h1 className="text-2xl font-bold underline">Privacy Policy</h1>
            <p>Hello, welcome to my guestbook. This is the information we collect:</p>
            <ul>
                <li>Email (NEVER SHOWN) - Only for email replies</li>
                <li>Discord username/id - Your name is published on your guestbook message. The ID is used for verifying who you are.</li>
                <li>Country that you live in - Analytics <Link className="underline" href="https://umami.is">(umami)</Link></li>
                <br />
                <br />
                <Link href="https://discord.com/privacy" className="underline">Note: Since we use Discord for auth, please go to their privacy policy for more.</Link>
            </ul>
        </main>
    </>
}

export default PrivacyPolicy;