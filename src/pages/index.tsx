import { useSession } from "next-auth/react";
import { useState } from "react";
import { trpc } from "../utils/trpc";
import { Toaster } from "react-hot-toast";
import Filter from 'bad-words';
import { error, success } from "../components/Notification";
import Spinner from "../components/Spinner";
import { CgTrash } from 'react-icons/cg'

interface MessageAuthorDetailsProps {
  msg: {
    name: string;
    message: string;
    authorEmail: string;
    id: string;
  }

  isAdmin: boolean | undefined;
}

const MessageAuthorDetails: React.FC<MessageAuthorDetailsProps> = ({ isAdmin, msg }) => {
  const utils = trpc.useContext();
  const deleteMessage = trpc.guestbook.deleteMessage.useMutation({
    onMutate: () => {
      utils.guestbook.getAll.cancel();
      const optimisticUpdate = utils.guestbook.getAll.getData();

      if (optimisticUpdate) {
        utils.guestbook.getAll.setData(undefined, optimisticUpdate);
      }
    },
    onSettled: () => {
      success({ title: 'Success!', text: 'Successfully deleted the selected message!    ' })
      utils.guestbook.getAll.invalidate();
    },
  });

  if (isAdmin === true) {
    return (
      <>
        <div className="flex">
          <span>- {msg.name}</span>
            <span className="text-gray-500"> ({msg.authorEmail}) </span>
            <span className="cursor-pointer" onClick={() => deleteMessage.mutate({
              id: msg.id
            })}><span className="underline"><CgTrash size={24} /></span>
          </span>
        </div>
        
      </>
    )
  }

  return <span>- {msg.name}</span>

}

interface MessageProps {
  msg: {
    name: string;
    message: string;
    authorEmail: string;
    id: string;
  }

  index: number;
}

const Message: React.FC<MessageProps> = ({ msg, index }) => {
  const { data: session, status } = useSession();
  const { data: isAdmin } = trpc.auth.getIsAdmin.useQuery({ userId: session?.user?.id });

  if (status === 'loading') {
    return <Spinner />
  }

  return (
    <div key={index} className="py-4">
      <div className="flex flex-col">
        <span>{msg.message}</span>
      </div>
      <MessageAuthorDetails isAdmin={isAdmin} msg={msg} />
    </div>
  )
}

const Messages = () => {
  const { data: messages, isLoading } = trpc.guestbook.getAll.useQuery();

  if (isLoading) return <Spinner />;

  return (
    <div>
      {messages?.map((msg, index) => {
        return (
          <Message msg={msg} index={index} key={index} />
        )
      })}
    </div>
  );
};

const Form = () => {
  const { data: session } = useSession()
  const [message, setMessage] = useState("");
  const utils = trpc.useContext();
  const postMessage = trpc.guestbook.postMessage.useMutation({
    onMutate: () => {
      utils.guestbook.getAll.cancel();
      const optimisticUpdate = utils.guestbook.getAll.getData();

      if (optimisticUpdate) {
        utils.guestbook.getAll.setData(undefined, optimisticUpdate);
      }
    },
    onError(err) {
      error({ title: 'Server Error', text: err.message });
    },
    onSuccess: () => {
      success({ title: 'Success!', text: 'Successfully published your message!     ' })
    },
    onSettled: () => {
      utils.guestbook.getAll.invalidate();
    },
  });

  return (
    <form
      className="flex gap-2"
      onSubmit={(event) => {
        event.preventDefault();
        const filter = new Filter();
        const english = /^[A-Za-z0-9_!?., ]*$/;
        const minLength = 10;

        if (message.length < minLength) {
          return error({ title: 'Error', text: `The post needs to be ${minLength.toString()} characters long` })
        }

        if (filter.isProfane(message)) {
          return error({ title: 'Error', text: 'Please refrain from using inappropriate language.' })
        }

        if (!english.test(message)) {
          return error({ title: 'Error', text: 'English is the only language allowed on this site.' });
        }

        if (session !== null) {
          postMessage.mutate({
            name: session.user?.name as string,
            email: session.user?.email as string,
            message,
          });
        }

        setMessage("");
      }}
    >
      <input
        type="text"
        value={message}
        placeholder="Your message..."
        minLength={2}
        maxLength={100}
        onChange={(event) => setMessage(event.target.value)}
        className="px-4 py-2 rounded-md border-2 border-zinc-800 bg-neutral-900 focus:outline-none"
      />
      <button
        type="submit"
        className="p-2 rounded-md border-2 border-zinc-800 focus:outline-none"
      >
        Submit
      </button>
    </form>
  );
}

const Home = () => {
  const { data: session, status } = useSession();

  if (status === "loading") {
    return <main className="flex flex-col items-center pt-4">
      <Spinner />
    </main>;
  }

  return (
    <main className="flex px-5">
      <Toaster />

      <div>
        <div>
          {session ? (
            <>
              <div className="pt-6">
                <Form />
              </div>
            </>
          ) : (
            <></>
          )}
          <div className="pt-2">
            <Messages />
          </div>
        </div>
      </div>
    </main>
  );
};

export default Home;
